#!/usr/bin/python
# -*- coding:utf-8 -*-

import sys
import os
import RPi.GPIO as GPIO
import cv2
import numpy as np

import tensorflow as tf
picdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'pic')
libdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'lib')
vegdetect = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'VegeDetection')

if os.path.exists(libdir):
    sys.path.append(libdir)
#if os.path.exists(objdetect):
#    sys.path.append(objdetect)
if os.path.exists(vegdetect):
    sys.path.append(vegdetect)
    sys.path.append(os.path.join(vegdetect,'CuttingMeasurement'))
    sys.path.append(os.path.join(vegdetect,'src'))
    sys.path.append(os.path.join(vegdetect,'src', 'image_classification'))

from waveshare_1in8_LCD import LCD_1in8
from waveshare_1in8_LCD import LCD_Config

from PIL  import Image
from PIL import ImageDraw
from PIL import ImageFont
from PIL import ImageColor

import ObjectDetect as objdet
import TestModel as vegdet
#import motor_control as motorctrl
#from image_classification import TestModel as vegdet
import motor_test
def main():
    LCD = LCD_1in8.LCD()
    print ("**********Init LCD**********")
    Lcd_ScanDir = LCD_1in8.SCAN_DIR_DFT  #SCAN_DIR_DFT = D2U_L2R
    LCD.LCD_Init(Lcd_ScanDir)
    LCD.LCD_Clear(0xffff);
    image = Image.new("RGB", (LCD.LCD_Dis_Column, LCD.LCD_Dis_Page), "WHITE")
    draw = ImageDraw.Draw(image)
    font18 = ImageFont.truetype(picdir+'/Font.ttc', 40)
    
    # to display the start screen
    def start_screen():
        image = Image.open(picdir+'/start_screen.bmp')
        print("AM HERE")
        LCD.LCD_ShowImage(image)
    
    # to display message when the vegetable has not been placed yet
    def waiting_for_veg():
        image = Image.open(picdir+'/waitingforvegetable.bmp')
        LCD.LCD_ShowImage(image)

    def display_image(file_name):
        print('file_name')
        image = Image.open(picdir+'/'+file_name+'.bmp')
        LCD.LCD_ShowImage(image)
    
    def yes_no_result(file_prefix):
        selected_option = True
        while True:
            confirm_flag = False
            display_image(file_prefix+'_yes')
            while GPIO.input(LCD_Config.BUTTON_1) is GPIO.LOW and confirm_flag is False:
                if GPIO.input(LCD_Config.BUTTON_2) is GPIO.HIGH:
                    confirm_flag = True
                    LCD_Config.Driver_Delay_ms(200)
                    break
                print('not pressed')
            if confirm_flag is True:
                break
            LCD_Config.Driver_Delay_ms(500)
            print('button pressed')
            display_image(file_prefix+'_no')
            while GPIO.input(LCD_Config.BUTTON_1) is GPIO.LOW and confirm_flag is False:
                if GPIO.input(LCD_Config.BUTTON_2) is GPIO.HIGH:
                    confirm_flag = True
                    LCD_Config.Driver_Delay_ms(200)
                    break
                print('not pressed')
            if confirm_flag is True:
                selected_option = False
                break
            LCD_Config.Driver_Delay_ms(500)
            print('button pressed')
        return selected_option

    def final_chopping_style(style_name):
        final_confirmation = yes_no_result('confirm')
        if not final_confirmation:
            return False
        display_image('in_process')
        # ------------------------------FIX THIS--------------------------------------
        style_nm_change = {'sdice':'small dice', 'mdice':'medium dice', 'ldice':'large dice', 'bat':'large dice', 'half_slice':'half_slice', 'full_slice':'full_slice'}
        # call the function to start the chopping assembly for this style
        #(x_coord, y_coord) = objdet.display_meas('/home/pi/ece445_project/display_code/VegeDetection/src/image_classification/imgCap.jpg', 100)
        #print("Got here",x_coord, y_coord)
        #---------------------------FIX THIS--------------------------------
        #motorctrl.start(x_coord, y_coord, style_nm_change[style_name], 0, 100, 100)
        motor_test.start_chopping()
        LCD_Config.Driver_Delay_ms(2000)
        display_image('complete')
        return True

    def use_chopping_menu():
        result = yes_no_result('other_menu')
        if result:
            let_cab_menu()
            return
        entire_menu = ['sdice', 'mdice', 'ldice', 'bat']
        confirm_flag = False
        curr_option = 0
        while True:
            display_image('styles_'+entire_menu[curr_option])
            while GPIO.input(LCD_Config.BUTTON_1) is GPIO.LOW and confirm_flag is False:
                if GPIO.input(LCD_Config.BUTTON_2) is GPIO.HIGH:
                    confirm_flag = True
                    LCD_Config.Driver_Delay_ms(200)
                    break
                print('not pressed')
            if confirm_flag:
                break
            else:
                curr_option = (curr_option + 1)%4
            LCD_Config.Driver_Delay_ms(200)
        return final_chopping_style(entire_menu[curr_option])

    def let_cab_menu():
        menu = ['half_slice', 'full_slice', 'viewall']
        confirm_flag = False
        curr_option = 0
        while True:
            display_image('suggestedchop_letcab_'+menu[curr_option])
            while GPIO.input(LCD_Config.BUTTON_1) is GPIO.LOW and confirm_flag is False:
                if GPIO.input(LCD_Config.BUTTON_2) is GPIO.HIGH:
                    confirm_flag = True
                    LCD_Config.Driver_Delay_ms(200)
                    break
                print('not pressed')
            if confirm_flag:
                LCD_Config.Driver_Delay_ms(200)
                break
            else:
                curr_option = (curr_option + 1)%3
            LCD_Config.Driver_Delay_ms(200)
        if curr_option is len(menu)-1:
            use_chopping_menu()
        else:
            final_chopping_style(menu[curr_option])
    
    start_screen()
    LCD_Config.Driver_Delay_ms(4000)
    
    while True:
        LCD_Config.Driver_Delay_ms(4000)
        display_image('placed_veg_yes')
        while GPIO.input(LCD_Config.BUTTON_2) is GPIO.LOW:
            print("waiting for vegetable")
        # while not object_detected(): # receive input from the object detection program
        # LCD_Config.Driver_Delay_ms(3000) # comment this line when the previous line is uncommented
        
        detected_veg_list = ['cabbage', 'cucumber', 'lettuce', 'onion', 'potato', 'tomato']
        suggested_chop = {'cucumber':'sdice', 'onion':'ldice', 'potato':'mdice', 'tomato':'bat'}
        DETECTED_VEG = vegdet.test_run()
        DETECTED_VEG = DETECTED_VEG.split()[0]
        DETECTED_VEG = DETECTED_VEG.lower()
        if DETECTED_VEG not in detected_veg_list:
            DETECTED_VEG = 'unknown'

        continue_chopping = False
        if DETECTED_VEG is not 'unknown':
            result = yes_no_result('vegdetect_'+DETECTED_VEG)
            if result:
                if DETECTED_VEG is not 'cabbage' and DETECTED_VEG is not 'lettuce':
                    accept_suggestion = yes_no_result('suggestedchop_'+suggested_chop[DETECTED_VEG])
                    if accept_suggestion:
                        continue_chopping = yes_no_result('confirm')
                        final_chopping_style(suggested_chop[DETECTED_VEG])
                        # while call to objectdetection shows something
                        LCD_Config.Driver_Delay_ms(2000)
                        continue # go back to waiting for veg once the vegetable has been removed
                    else:
                        while not use_chopping_menu():
                            print("showing the chopping menu again")
                else:
                    let_cab_menu()
                    continue
            else:
                while not use_chopping_menu():
                    print("showing the chopping menu again")
        else:
            while not use_chopping_menu():
                print("showing the chopping menu again")

#try:
if __name__ == '__main__':
    main()
#except:
#   print("except")
