import RPi.GPIO as GPIO
from RpiMotorLib import RpiMotorLib
import time

motor1_dir = 4
motor1_step = 23
motor2_dir = 22
motor2_step = 16

# GPIO.setmode(GPIO.BCM)

# GPIO.setup(2, GPIO.OUT)
def start_choppinh():
p = GPIO.PWM(2, 50)

p.start(2.5)


motor_x = RpiMotorLib.A4988Nema(motor1_dir, motor1_step, (27,27,27), "A4988")
motor_y = RpiMotorLib.A4988Nema(motor2_dir, motor2_step, (27,27,27), "A4988")

motor_y.motor_go(True, "Full", 563, 0.003, False, 0.1)
motor_x.motor_go(False, "Full", 600, 0.008, False, 0.1)



start_time = time.time()
endtime = start_time + 2
while(time.time()<= endtime):
    print("woohoo")
# 
motor_y.motor_go(False, "Full", 484, 0.003, False, 0.1)
motor_x.motor_go(True, "Full", 614, 0.008, False, 0.1)
# 


start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")
motor_y.motor_go(False, "Full", 128, 0.008, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")

motor_x.motor_go(False, "Full", 64, 0.003, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")


motor_y.motor_go(True, "Full", 128, 0.008, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")

motor_x.motor_go(False, "Full", 64, 0.003, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")

motor_y.motor_go(False, "Full", 128, 0.008, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")

motor_x.motor_go(False, "Full", 64, 0.003, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")

motor_y.motor_go(True, "Full", 128, 0.008, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")

motor_x.motor_go(False, "Full", 64, 0.003, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")

motor_y.motor_go(False, "Full", 128, 0.008, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")

p.ChangeDutyCycle(7.5)


start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")
motor_y.motor_go(True, "Full", 175, 0.008, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")

motor_x.motor_go(True, "Full", 64, 0.003, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")


motor_y.motor_go(False, "Full", 150, 0.008, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")

motor_x.motor_go(True, "Full", 64, 0.003, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")
    
motor_y.motor_go(True, "Full", 150, 0.008, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")

motor_x.motor_go(True, "Full", 64, 0.003, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")

motor_y.motor_go(False, "Full", 150, 0.008, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")

motor_x.motor_go(True, "Full", 64, 0.003, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")

motor_y.motor_go(True, "Full", 150, 0.008, False, 0.1)
start_time = time.time()
endtime = start_time+ 18
while(time.time()<= endtime):
    print("woohoo")

p.ChangeDutyCycle(2.5)

motor_y.motor_go(True, "Full", 627, 0.003, False, 0.1)
motor_x.motor_go(False, "Full", 450, 0.008, False, 0.1)
start_time = time.time()
endtime = start_time+ 2
while(time.time()<= endtime):
    print("woohoo")
motor_y.motor_go(False, "Full", 563, 0.003, False, 0.1)
motor_x.motor_go(True, "Full", 450, 0.008, False, 0.1)
print("done")
GPIO.cleanup()
