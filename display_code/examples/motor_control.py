import RPi.GPIO as GPIO
from RpiMotorLib import RpiMotorLib
import time

LAR_Pin = 18
motor1_dir = 19
motor1_step = 20
motor2_dir = 21
motor2_step = 5

motor_x = RpiMotorLib.A4988Nema(motor1_dir, motor1_step, (20,20,20), "A4988")
motor_y = RpiMotorLib.A4988Nema(motor2_dir, motor2_step, (20,20,20), "A4988")
#GPIO.setup(LAR_Pin, GPIO.OUT)

def start(dist_x, dist_y, dice_type, offset, length, width):
    if dice_type == "small dice":
        small_dice(offset, length, width)
    elif dice_type == "large dice":
        large_dice(offset, length, width)
    elif dice_type == "medium dice":
        medium_dice(offset, length, width)
    elif dice_type == "end":
    #add math to convert dist to reset position 
        motor_x.motor_go(True, "Full", 1200, 0, False, 0)
        motor_y.motor_go(True, "Full", 600, 0, False, 0)
        return True
    else:
        motor_x.motor_go(True, "Full", 1200, 0, False, 0)
        motor_y.motor_go(True, "Full", 600, 0, False, 0)
    

def blade_ext(down):
    start_time = time.time()
    endtime = start_time+4.3
    while(time.time()<= endtime):
        if(down):
            GPIO.output(LAR_Pin, GPIO.LOW)
        else:
            GPIO.output(LAR_Pin, GPIO.HIGH)

def small_dice(offset, length, width):
    motor_x.motor_go(False, "Full", 1200, 0, False, 0)
    motor_y.motor_go(False, "Full", 600, 0, False, 0)
    dist_cut_x = 0.0
    dist_cut_y = 0.0
    num_y_steps = 0
    if width>=1:
        motor_y.motor_go(False, "Full", 100, 0, False, 0)
        while(dist_cut_x <= length):
            dist_cut_x = 0.0
            dist_cut_y = 0.0
            num_y_steps = 0
            blade_ext(True)
            blade_ext(False)
            dist_cut_y += 1
            while(dist_cut_y <= width):
                motor_y.motor_go(True, "Full", 100, 0, False, 0)
                blade_ext(True)
                blade_ext(False)
                num_y_steps += 1
                dist_cut_y += 1
            motor_y.motor_go(True, "Full", num_y_steps*100, 0, False, 0)
            motor_x.motor_go(False, "Full", 34, 0, False, 0)
            blade_ext(True)
            blade_ext(False)
            dist_cut_x += 0.25
    else:
        blade_ext(True)
        blade_ext(False)
        while(dist_cut_x <= length):
            motor_x.motor_go(False, "Full", 34, 0, False, 0)
            blade_ext(True)
            blade_ext(False)
            dist_cut_x += 0.25

    #add rotation and conitnue
    start(dist_cut_x, dist_cut_y, "end", 0,0,0)

def large_dice(offset, length, width):
    motor_x.motor_go(False, "Full", 1200, 0, False, 0)
    motor_y.motor_go(False, "Full", 600, 0, False, 0)
    dist_cut_x = 0.0
    dist_cut_y = 0.0
    num_y_steps = 0
    if width>=1:
        motor_y.motor_go(False, "Full", 100, 0, False, 0)
        while(dist_cut_x <= length):
            dist_cut_x = 0.0
            dist_cut_y = 0.0
            num_y_steps = 0
            blade_ext(True)
            blade_ext(False)
            dist_cut_y += 1
            while(dist_cut_y <= width):
                motor_y.motor_go(True, "Full", 100, 0, False, 0)
                blade_ext(True)
                blade_ext(False)
                num_y_steps += 1
                dist_cut_y += 1
            motor_y.motor_go(True, "Full", num_y_steps*100, 0, False, 0)
            motor_x.motor_go(False, "Full", 34, 0, False, 0)
            blade_ext(True)
            blade_ext(False)
            dist_cut_x += 0.25
    else:
        blade_ext(True)
        blade_ext(False)
        while(dist_cut_x <= length):
            motor_x.motor_go(False, "Full", 34, 0, False, 0)
            blade_ext(True)
            blade_ext(False)
            dist_cut_x += 0.25
    start(dist_cut_x, dist_cut_y, "end", 0,0,0)

def medium_dice(offset, length, width):
    motor_x.motor_go(False, "Full", 1200, 0, False, 0)
    motor_y.motor_go(False, "Full", 600, 0, False, 0)
    dist_cut_x = 0.0
    dist_cut_y = 0.0
    num_y_steps = 0
    if width>=1:
        motor_y.motor_go(False, "Full", 100, 0, False, 0)
        while(dist_cut_x <= length):
            dist_cut_x = 0.0
            dist_cut_y = 0.0
            num_y_steps = 0
            blade_ext(True)
            blade_ext(False)
            dist_cut_y += 1
            while(dist_cut_y <= width):
                motor_y.motor_go(True, "Full", 100, 0, False, 0)
                blade_ext(True)
                blade_ext(False)
                num_y_steps += 1
                dist_cut_y += 1
            motor_y.motor_go(True, "Full", num_y_steps*100, 0, False, 0)
            motor_x.motor_go(False, "Full", 34, 0, False, 0)
            blade_ext(True)
            blade_ext(False)
            dist_cut_x += 0.25
    else:
        blade_ext(True)
        blade_ext(False)
        while(dist_cut_x <= length):
            motor_x.motor_go(False, "Full", 34, 0, False, 0)
            blade_ext(True)
            blade_ext(False)
            dist_cut_x += 0.25
    start(dist_cut_x, dist_cut_y, "end", 0,0,0)